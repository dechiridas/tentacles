# Tentacles
This is my little pet project. It's an image viewer for local files, *booru image boards and 4chan. Maybe I'll add
reddit too? Right now the code is very much nasty spaghetti and I wouldn't recommend reading it unless you're a gluten
for punishment.

## Requirements
|Library|Version|
|--------|---------|
|Java    |[10.0.2](http://www.oracle.com/technetwork/java/javase/downloads/jdk10-downloads-4416644.html)|
|Guava   |[25.1](https://mvnrepository.com/artifact/com.google.guava/guava/25.1-jre)|
|org.json|[20180130](https://mvnrepository.com/artifact/org.json/json/20180130)|
|Jsoup   |[1.11.3](https://mvnrepository.com/artifact/org.jsoup/jsoup/1.11.3)|