package org.dech.tentacles;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("views/Tentacles.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(root, 1024, 768);
        primaryStage.setTitle("Tentacles image viewer");
        primaryStage.setScene(scene);

        primaryStage.setMinWidth(1024);
        primaryStage.setMinHeight(768);

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
