package org.dech.tentacles.controllers;

import com.google.common.io.Files;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;
import org.dech.tentacles.handler.AbsGallery;
import org.dech.tentacles.handler.impl.FourChanGallery;
import org.dech.tentacles.handler.impl.GelbooruGallery;
import org.dech.tentacles.handler.impl.LocalGallery;
import org.dech.tentacles.util.Board;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ImageView imageView;
    public AnchorPane imageViewPane;
    public FlowPane thumbnailPane;

    public TextField webSearchInput;
    public Button btnPreviousPage;
    public Button btnNextPage;
    public ScrollPane scrollPane;
    public TextField textUrl;
    public Button btnSaveImage;
    public Button btnShowInBrowser;
    public ComboBox<Board> boardSelection;
    public Label textPage;
    public Label textImageCount;
    public Label textTitle;

    private String currentFileExtension = "";
    private String currentFilename = "";
    private String boardTitle = "";
    private String board = "v";
    private AbsGallery gallery;

    public enum ViewType {
        LOCALFILES,
        GELBOORU,
        BOARD_4CHAN,
        THREAD_4CHAN
    }

    private ViewType currentViewType;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        webSearchInput.focusedProperty().addListener((obs, oldValue, newValue) -> handleInputFocus(newValue));
        thumbnailPane.setMaxHeight(10000);
        thumbnailPane.setPadding(new Insets(10, 0, 10, 0));

        boardSelection.setItems(FXCollections.observableArrayList(Board.boards));
        boardSelection.setValue(boardSelection.getItems().get(0));

        boardSelection.setConverter(new StringConverter<Board>() {
            @Override
            public String toString(Board board) {
                return board.getName();
            }

            @Override
            public Board fromString(String string) {
                return null;
            }
        });

        boardSelection.valueProperty().addListener((ChangeListener) (observable, oldValue, newValue) -> {
            board = ((Board)newValue).getCode();
            boardTitle = ((Board)newValue).getName();
        });
    }

    public void loadFolder(ActionEvent actionEvent) {
        gallery = new LocalGallery(this);
        ((LocalGallery)gallery).loadLayerOne(actionEvent);
    }

    public void searchGelbooru(ActionEvent actionEvent) {
        gallery = new GelbooruGallery(this);
        ((GelbooruGallery)gallery).loadLayerOne(actionEvent);
    }

    public void search4chanBoard(ActionEvent actionEvent) {
        gallery = new FourChanGallery(this, board);
        ((FourChanGallery)gallery).loadLayerOne(actionEvent);
    }

    public void saveImage(ActionEvent actionEvent) throws IOException {
        Image image = imageView.getImage();
        BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);

        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialFileName(String.format("%s.%s", currentFilename, currentFileExtension));
        File file = fileChooser.showSaveDialog(((Node)actionEvent.getTarget()).getScene().getWindow());

        if (file != null) {
            ImageIO.write(bufferedImage, currentFileExtension, file);
        }
    }

    public void nextPage(ActionEvent actionEvent) {
        if (currentViewType == ViewType.GELBOORU) {
            ((GelbooruGallery)gallery).nextPage(actionEvent);
        } else if (currentViewType == ViewType.BOARD_4CHAN) {
            ((FourChanGallery)gallery).nextPage(actionEvent);
        }
    }

    public void previousPage(ActionEvent actionEvent) {
        if (currentViewType == ViewType.GELBOORU) {
            ((GelbooruGallery)gallery).previousPage(actionEvent);
        } else if (currentViewType == ViewType.BOARD_4CHAN) {
            ((FourChanGallery)gallery).previousPage(actionEvent);
        }
    }

    public void cleanImageScrollPane() {
        thumbnailPane.getChildren().clear();
        scrollPane.setVvalue(0);
    }

    public void showImage(String url) {
        if (currentViewType == ViewType.BOARD_4CHAN) {
            ((FourChanGallery) gallery).loadLayerTwo(url, boardTitle);
        } else {
            if (currentViewType == ViewType.LOCALFILES) {
                url = url.replace("file:///", "").replace("\\", "/");
            }

            textUrl.setText(url);

            btnSaveImage.setDisable(currentViewType == ViewType.LOCALFILES);
            btnShowInBrowser.setDisable(currentViewType == ViewType.LOCALFILES);

            currentFileExtension = Files.getFileExtension(url);
            currentFilename = Files.getNameWithoutExtension(url);

            try {
                if (url.startsWith("http")) {
                    imageView.setImage(new Image(url, true));
                } else {
                    imageView.setImage(new Image(new FileInputStream(url)));
                }
                imageView.setFitHeight(imageViewPane.getHeight());
                imageView.setFitWidth(imageViewPane.getWidth());
                imageView.setPreserveRatio(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void handleInputFocus(boolean isFocused) {
        toggleNode(btnPreviousPage, isFocused);
        toggleNode(btnNextPage, isFocused);
    }

    private void toggleNode(Node node, boolean show) {
        if (show) {
            node.setManaged(false);
            node.setVisible(false);
        } else {
            node.setManaged(true);
            node.setVisible(true);
        }
    }

    public FlowPane getThumbnailPane() {
        return thumbnailPane;
    }

    public String getBoardTitle() {
        return boardTitle;
    }

    public TextField getTextUrl() {
        return textUrl;
    }

    public Label getTextPage() {
        return textPage;
    }

    public Label getTextTitle() {
        return textTitle;
    }

    public Label getTextImageCount() {
        return textImageCount;
    }

    public Button getBtnPreviousPage() {
        return btnPreviousPage;
    }

    public Button getBtnNextPage() {
        return btnNextPage;
    }

    public void setCurrentViewType(ViewType currentViewType) {
        this.currentViewType = currentViewType;
    }

    public TextField getWebSearchInput() {
        return webSearchInput;
    }

    public Button getBtnSaveImage() {
        return btnSaveImage;
    }

    public Button getBtnShowInBrowser() {
        return btnShowInBrowser;
    }
}
