package org.dech.tentacles.handler;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.css.PseudoClass;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import org.dech.tentacles.controllers.Controller;

public abstract class AbsGallery {
    protected Controller controller;

    protected void buildLayerOneWrapper(String imageUrl, String thumbnailUrl) {
        Image thumbnail = new Image(thumbnailUrl, 100, 100, true, false, true);
        ImageView thumbnailView = new ImageView(thumbnail);

        PseudoClass border = PseudoClass.getPseudoClass("border");
        HBox wrapper = new HBox();

        wrapper.getChildren().add(thumbnailView);
        wrapper.setMinHeight(100);
        wrapper.setMinWidth(100);
        wrapper.alignmentProperty().setValue(Pos.CENTER);
        wrapper.getStyleClass().add("scroll-pane-image");

        BooleanProperty borderActive = new SimpleBooleanProperty() {
            @Override
            protected void invalidated() {
                wrapper.pseudoClassStateChanged(border, get());
            }
        };

        wrapper.setOnMouseClicked(evt -> controller.showImage(imageUrl));
        wrapper.setOnMouseEntered(evt -> borderActive.set(true));
        wrapper.setOnMouseExited(evt -> borderActive.set(false));

        controller.getThumbnailPane().getChildren().add(wrapper);
    }

    public abstract void buildLayerOneWrapper(String threadUrl, String thumbnailUrl, String title);
}
