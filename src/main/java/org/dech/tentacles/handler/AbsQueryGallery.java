package org.dech.tentacles.handler;

public abstract class AbsQueryGallery extends AbsGallery {
    protected String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
