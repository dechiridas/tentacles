package org.dech.tentacles.handler;

import javafx.stage.Window;

import java.io.File;

public interface ILocalGallery {
    File[] selectDirectoryAndGetFiles(Window window);
}
