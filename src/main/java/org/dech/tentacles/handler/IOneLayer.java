package org.dech.tentacles.handler;

import javafx.event.ActionEvent;

public interface IOneLayer {
    void loadLayerOne(ActionEvent actionEvent);
}
