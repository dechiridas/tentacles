package org.dech.tentacles.handler;

import javafx.event.ActionEvent;

public interface IPaged {
    void nextPage(ActionEvent actionEvent);
    void previousPage(ActionEvent actionEvent);
}
