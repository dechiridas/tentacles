package org.dech.tentacles.handler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

public interface IRemoteGallery {
    static JSONObject getJSONObjectFromUrl(String url) throws IOException {
        InputStream is = new URL(url).openStream();
        JSONObject json;

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(reader).trim();
            json = new JSONObject(jsonText);
        } finally {
            is.close();
        }

        return json;
    }

    static JSONArray getJSONArrayFromUrl(String url) throws IOException {
        InputStream is = new URL(url).openStream();
        JSONArray json;

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(reader).trim();
            json = new JSONArray(jsonText);

        } finally {
            is.close();
        }

        return json;
    }

    static String readAll(Reader reader) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = reader.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
