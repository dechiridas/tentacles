package org.dech.tentacles.handler;

public interface ITwoLayer extends IOneLayer {
    void loadLayerTwo(String url, String title);
}
