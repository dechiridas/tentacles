package org.dech.tentacles.handler.impl;

import com.google.common.io.Files;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;
import org.dech.tentacles.controllers.Controller;
import org.dech.tentacles.handler.AbsGallery;
import org.dech.tentacles.handler.IPaged;
import org.dech.tentacles.handler.IRemoteGallery;
import org.dech.tentacles.handler.ITwoLayer;
import org.dech.tentacles.util.BaseUrls;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;

public class FourChanGallery extends AbsGallery implements ITwoLayer, IPaged, IRemoteGallery {
    private String board;
    private int page = 1;

    public FourChanGallery(Controller controller, String board) {
        this.controller = controller;
        this.board = board;
    }

    @Override
    public void nextPage(ActionEvent actionEvent) {
        page++;
        loadLayerOne(actionEvent);
    }

    @Override
    public void previousPage(ActionEvent actionEvent) {
        if (page > 1)
            page--;
        loadLayerOne(actionEvent);
    }

    @Override
    public void loadLayerOne(ActionEvent actionEvent) {
        controller.setCurrentViewType(Controller.ViewType.BOARD_4CHAN);

        controller.getBtnPreviousPage().setDisable(false);
        controller.getBtnNextPage().setDisable(false);
        controller.getBtnSaveImage().setDisable(false);
        controller.getBtnShowInBrowser().setDisable(false);

        controller.cleanImageScrollPane();

        String url = BaseUrls.baseUrl4ChanBoard
                .replace("%board%", board)
                .replace("%pagenumber%", page + "");

        try {
            JSONObject json = IRemoteGallery.getJSONObjectFromUrl(url);
            JSONArray threads = json.getJSONArray("threads");

            controller.getTextUrl().setText(url);
            controller.getTextTitle().setText(controller.getBoardTitle());
            controller.getTextPage().setText(page + "");
            controller.getTextImageCount().setText(json.length() + "");

            for (int i = 0; i < threads.length(); i++) {
                JSONObject object = threads.getJSONObject(i);
                JSONObject firstPost = object.getJSONArray("posts").getJSONObject(0);

                Long threadId = firstPost.getLong("no");
                String threadTitle = firstPost.has("com") ? Jsoup.parse(firstPost.getString("com")).text() : "";
                String thumbnailUrl = firstPost.has("tim") ? create4ChanThumbnailUrl(String.format("https://i.4cdn.org/%s/%ss%s", board, firstPost.getLong("tim"), firstPost.getString("ext"))) : "";
                String imageUrl = firstPost.has("tim") ? String.format("https://is2.4chan.org/%s/%s%s", board, firstPost.getLong("tim"), firstPost.getString("ext")) : "";

                String threadUrl = BaseUrls.baseUrl4ChanThread
                        .replace("%board%", board)
                        .replace("%thread%", threadId + "");

                if (Files.getFileExtension(imageUrl).matches("jp(e)?g|png")) {
                    buildLayerOneWrapper(threadUrl, thumbnailUrl, threadTitle);
                }

                //    https://i.4cdn.org/v/1533410757361s.jpg - thumbnail
                // https://is2.4chan.org/v/1533410757361.jpg - full size
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void buildLayerOneWrapper(String threadUrl, String thumbnailUrl, String title) {
        Image thumbnail = new Image(thumbnailUrl, 100, 100, true, false, true);
        ImageView thumbnailView = new ImageView(thumbnail);

        PseudoClass border = PseudoClass.getPseudoClass("border");
        HBox wrapper = new HBox();

        Label labelTitle = new Label(title.substring(0, Math.min(100, title.length())) + (title.length() > 100 ? "..." : ""));
        labelTitle.maxWidth(200);
        labelTitle.prefWidth(200);
        labelTitle.setWrapText(true);
        labelTitle.setTextAlignment(TextAlignment.LEFT);
        labelTitle.setContentDisplay(ContentDisplay.TOP);
        labelTitle.setGraphic(thumbnailView);
        labelTitle.paddingProperty().set(new Insets(0, 5,0,0));

        wrapper.setMinHeight(100);
        wrapper.setMaxHeight(200);

        wrapper.setMinWidth(100);
        wrapper.setMaxWidth(200);

        wrapper.getChildren().add(labelTitle);

        wrapper.alignmentProperty().setValue(Pos.CENTER);
        wrapper.getStyleClass().add("scroll-pane-image");

        BooleanProperty borderActive = new SimpleBooleanProperty() {
            @Override
            protected void invalidated() {
                wrapper.pseudoClassStateChanged(border, get());
            }
        };

        wrapper.setOnMouseClicked(evt -> loadLayerTwo(threadUrl, title));
        wrapper.setOnMouseEntered(evt -> borderActive.set(true));
        wrapper.setOnMouseExited(evt -> borderActive.set(false));

        controller.getThumbnailPane().getChildren().add(wrapper);
    }

    @Override
    public void loadLayerTwo(String url, String title) {
        controller.getTextTitle().setText(title);
        load4chanThread(url);
    }

    private void load4chanThread(String url) {
        controller.setCurrentViewType(Controller.ViewType.THREAD_4CHAN);

        controller.getBtnPreviousPage().setDisable(true);
        controller.getBtnNextPage().setDisable(true);
        controller.getBtnSaveImage().setDisable(false);
        controller.getBtnShowInBrowser().setDisable(false);

        controller.cleanImageScrollPane();

        try {
            JSONObject json = IRemoteGallery.getJSONObjectFromUrl(url);
            JSONArray posts = json.getJSONArray("posts");

            controller.getTextPage().setText("");
            controller.getTextUrl().setText(url);

            int images = 0;

            for (int i = 0; i < posts.length(); i++) {
                JSONObject post = posts.getJSONObject(i);

                if (post.has("tim")) {
                    images++;

                    Long imageId = post.getLong("tim");
                    String extension = post.getString("ext");
                    String thumbnailUrl = create4ChanThumbnailUrl(String.format("https://i.4cdn.org/%s/%ds%s", board, imageId, extension));
                    String imageUrl = String.format("https://is2.4chan.org/%s/%d%s", board, imageId, extension);

                    if (Files.getFileExtension(imageUrl).matches("jp(e)?g|png")) {
                        buildLayerOneWrapper(imageUrl, thumbnailUrl);
                    }

                    if (i == 0)
                        controller.showImage(imageUrl);
                }
            }

            controller.getTextImageCount().setText(images + "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String create4ChanThumbnailUrl(String imageUrl) {
        return imageUrl
                .replace("jpeg", "jpg")
                .replace("png", "jpg");
    }
}
