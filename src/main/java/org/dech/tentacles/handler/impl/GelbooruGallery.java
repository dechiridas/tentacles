package org.dech.tentacles.handler.impl;

import javafx.event.ActionEvent;
import org.dech.tentacles.controllers.Controller;
import org.dech.tentacles.handler.AbsQueryGallery;
import org.dech.tentacles.handler.IOneLayer;
import org.dech.tentacles.handler.IPaged;
import org.dech.tentacles.handler.IRemoteGallery;
import org.dech.tentacles.util.BaseUrls;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

public class GelbooruGallery extends AbsQueryGallery implements IOneLayer, IPaged, IRemoteGallery {
    private int page = 0;

    public GelbooruGallery(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void loadLayerOne(ActionEvent actionEvent) {
        controller.setCurrentViewType(Controller.ViewType.GELBOORU);

        controller.getBtnNextPage().setDisable(false);
        controller.getBtnPreviousPage().setDisable(false);
        controller.getBtnSaveImage().setDisable(false);
        controller.getBtnShowInBrowser().setDisable(false);

        controller.cleanImageScrollPane();

        String url = BaseUrls.gelbooruBaseUrl + cleanGelbooruUrl(controller.getWebSearchInput().getText());
        url = url.replace("pid=0", String.format("pid=%d", page));

        try {
            JSONArray json = IRemoteGallery.getJSONArrayFromUrl(url);

            controller.getTextUrl().setText(url);
            controller.getTextTitle().setText("");
            controller.getTextPage().setText(page + "");
            controller.getTextImageCount().setText(json.length() + "");

            for (int i = 0; i < json.length(); i++) {
                JSONObject object = json.getJSONObject(i);

                String imageUrl = object.getString("file_url");
                String filename = object.getString("image");
                String thumbnailUrl = createGelbooruThumbnailUrl(imageUrl, filename);

                if (com.google.common.io.Files.getFileExtension(filename).matches("jp(e)?g|png")) {
                    buildLayerOneWrapper(imageUrl, thumbnailUrl);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void nextPage(ActionEvent actionEvent) {
        page++;
        loadLayerOne(actionEvent);
    }

    @Override
    public void previousPage(ActionEvent actionEvent) {
        if (page > 0)
            page--;
        loadLayerOne(actionEvent);
    }

    private static String cleanGelbooruUrl(String input) {
        return input.trim()
                .replace(" ", "+")
                .replace("&", "")
                .replace("=", "");
    }

    private static String createGelbooruThumbnailUrl(String imageUrl, String filename) {
        return imageUrl
                .replace("images", "thumbnails")
                .replace(filename, "thumbnail_" + filename)
                .replace("png", "jpg")
                .replace("jpeg", "jpg");
    }

    @Override
    public void buildLayerOneWrapper(String threadUrl, String thumbnailUrl, String title) {
        buildLayerOneWrapper(threadUrl, thumbnailUrl);
    }
}
