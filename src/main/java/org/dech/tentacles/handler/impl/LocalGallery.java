package org.dech.tentacles.handler.impl;

import com.google.common.io.Files;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import org.dech.tentacles.controllers.Controller;
import org.dech.tentacles.handler.AbsGallery;
import org.dech.tentacles.handler.ILocalGallery;
import org.dech.tentacles.handler.IOneLayer;

import java.io.File;

public class LocalGallery extends AbsGallery implements IOneLayer, ILocalGallery {
    public LocalGallery(Controller controller) {
        this.controller = controller;
    }

    @Override
    public File[] selectDirectoryAndGetFiles(Window window) {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Select folder");
        File defaultDirectory = new File("C:/");
        chooser.setInitialDirectory(defaultDirectory);
        File selectedDirectory = chooser.showDialog(window);

        return selectedDirectory.listFiles();
    }

    @Override
    public void loadLayerOne(ActionEvent actionEvent) {
        controller.setCurrentViewType(Controller.ViewType.LOCALFILES);

        controller.getBtnNextPage().setDisable(true);
        controller.getBtnPreviousPage().setDisable(true);
        controller.getBtnSaveImage().setDisable(true);
        controller.getBtnShowInBrowser().setDisable(true);

        controller.cleanImageScrollPane();

        File[] files = selectDirectoryAndGetFiles(((Node)actionEvent.getTarget()).getScene().getWindow());

        controller.getTextUrl().setText(files[0].getParent());
        controller.getTextPage().setText("");
        controller.getTextTitle().setText("");
        controller.getTextImageCount().setText(files.length + "");

        for (File file : files) {
            if (Files.getFileExtension(file.getAbsolutePath()).toLowerCase().matches("jp(e)?g|png")) {
                String imageUrl = "file:///" + file.getAbsolutePath();
                buildLayerOneWrapper(imageUrl, imageUrl);
            }
        }
    }

    @Override
    public void buildLayerOneWrapper(String threadUrl, String thumbnailUrl, String title) {
        buildLayerOneWrapper(threadUrl, thumbnailUrl);
    }
}
