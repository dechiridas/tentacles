package org.dech.tentacles.util;

public class BaseUrls {
    public static final String gelbooruBaseUrl = "https://gelbooru.com/index.php?page=dapi&s=post&q=index&json=1&limit=25&pid=0&tags=";
    public static final String baseUrl4ChanBoard = "https://a.4cdn.org/%board%/%pagenumber%.json";
    public static final String baseUrl4ChanThread = "https://a.4cdn.org/%board%/thread/%thread%.json";
}
