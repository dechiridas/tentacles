package org.dech.tentacles.util;

import java.util.Arrays;
import java.util.List;

public class Board {
    private String code;
    private String name;

    public static final List<Board> boards = Arrays.asList(
            new Board("3", "/3/ - 3DCG"),
            new Board("a", "/a/ - Anime &amp; Manga"),
            new Board("aco", "/aco/ - Adult Cartoons"),
            new Board("adv", "/adv/ - Advice"),
            new Board("an", "/an/ - Animals &amp; Nature"),
            new Board("asp", "/asp/ - Alternative Sports"),
            new Board("b", "/b/ - Random"),
            new Board("bant", "/bant/ - International/Random"),
            new Board("biz", "/biz/ - Business &amp; Finance"),
            new Board("c", "/c/ - Anime/Cute"),
            new Board("cgl", "/cgl/ - Cosplay &amp; EGL"),
            new Board("ck", "/ck/ - Food &amp; Cooking"),
            new Board("cm", "/cm/ - Cute/Male"),
            new Board("co", "/co/ - Comics &amp; Cartoons"),
            new Board("d", "/d/ - Hentai/Alternative"),
            new Board("diy", "/diy/ - Do It Yourself"),
            new Board("e", "/e/ - Ecchi"),
            new Board("f", "/f/ - Flash"),
            new Board("fa", "/fa/ - Fashion"),
            new Board("fit", "/fit/ - Fitness"),
            new Board("g", "/g/ - Technology"),
            new Board("gd", "/gd/ - Graphic Design"),
            new Board("gif", "/gif/ - Adult GIF"),
            new Board("h", "/h/ - Hentai"),
            new Board("hc", "/hc/ - Hardcore"),
            new Board("his", "/his/ - History &amp; Humanities"),
            new Board("hm", "/hm/ - Handsome Men"),
            new Board("hr", "/hr/ - High Resolution"),
            new Board("i", "/i/ - Oekaki"),
            new Board("ic", "/ic/ - Artwork/Critique"),
            new Board("int", "/int/ - International"),
            new Board("jp", "/jp/ - Otaku Culture"),
            new Board("k", "/k/ - Weapons"),
            new Board("lgbt", "/lgbt/ - LGBT"),
            new Board("lit", "/lit/ - Literature"),
            new Board("m", "/m/ - Mecha"),
            new Board("mlp", "/mlp/ - Pony"),
            new Board("mu", "/mu/ - Music"),
            new Board("n", "/n/ - Transportation"),
            new Board("news", "/news/ - Current News"),
            new Board("o", "/o/ - Auto"),
            new Board("out", "/out/ - Outdoors"),
            new Board("p", "/p/ - Photo"),
            new Board("po", "/po/ - Papercraft &amp; Origami"),
            new Board("pol", "/pol/ - Politically Incorrect"),
            new Board("qa", "/qa/ - Question &amp; Answer"),
            new Board("qst", "/qst/ - Quests"),
            new Board("r", "/r/ - Adult Requests"),
            new Board("r9k", "/r9k/ - ROBOT9001"),
            new Board("s", "/s/ - Sexy Beautiful Women"),
            new Board("s4s", "/s4s/ - Shit 4chan Says"),
            new Board("sci", "/sci/ - Science &amp; Math"),
            new Board("soc", "/soc/ - Cams &amp; Meetups"),
            new Board("sp", "/sp/ - Sports"),
            new Board("t", "/t/ - Torrents"),
            new Board("tg", "/tg/ - Traditional Games"),
            new Board("toy", "/toy/ - Toys"),
            new Board("trv", "/trv/ - Travel"),
            new Board("tv", "/tv/ - Television &amp; Film"),
            new Board("u", "/u/ - Yuri"),
            new Board("v", "/v/ - Video Games"),
            new Board("vg", "/vg/ - Video Game Generals"),
            new Board("vip", "/vip/ - Very Important Posts"),
            new Board("vp", "/vp/ - Pokémon"),
            new Board("vr", "/vr/ - Retro Games"),
            new Board("w", "/w/ - Anime/Wallpapers"),
            new Board("wg", "/wg/ - Wallpapers/General"),
            new Board("wsg", "/wsg/ - Worksafe GIF"),
            new Board("wsr", "/wsr/ - Worksafe Requests"),
            new Board("x", "/x/ - Paranormal"),
            new Board("y", "/y/ - Yaoi")
    );

    private Board(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
